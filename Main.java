
public class Main {

	public static void main(String[] args) {
		
		System.out.println("Hello!");
		
		Aluno aluno1, aluno2, aluno3;
		Planilha ufal = new Planilha();
		Turma p3 = new Turma( "P3" );
		
		double n1[] = { 10.0 , 10.0 , 10.0 };
		double n2[] = { 0.0 , 0.0 , 0.0 };
		double n3[] = { 7.0 , 7.0 , 7.0 };
		
		aluno1 = new Aluno( "A", "01", new Notas( n1 ) );
		aluno2 = new Aluno( "B", "02", new Notas( n2 ) );
		aluno3 = new Aluno( "C", "03", new Notas( n3 ) );
		
		p3.addAluno(aluno1);
		p3.addAluno(aluno2);
		p3.addAluno(aluno3);
		
		ufal.addTurma(p3);
		ufal.notas(0);
		System.out.println("--------------------");
		ufal.turmas.get(0).setPeso(5, 1, 4);
		ufal.notas(0);
	}
	
}
