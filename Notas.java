
public class Notas {
	
	double provas[] = new double[3];
	
	public Notas( double provas[] ) {
		this.provas = provas;
	}
	public double calculo( double p1, double p2, double p3 )
	{
		return p1*provas[0] + p2*provas[1] + p3* provas[2];
	}
}
