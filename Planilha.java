import java.util.ArrayList;

public class Planilha {

	public ArrayList< Turma > turmas;
	
	public Planilha() {
		turmas = new ArrayList< Turma >(); 
	}
	
	void addTurma( Turma turma ){
		turmas.add(turma);
	}
	
	void notas( int i ){
		turmas.get(i).view();
	}
	
}
