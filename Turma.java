import java.util.ArrayList;

public class Turma {

	String name;
	ArrayList<Aluno> alunos = new ArrayList<Aluno>();;
	ArrayList<Double> resultado = new ArrayList<Double>();
	double p1, p2, p3;
	
	public Turma() {
		// TODO Auto-generated constructor stub
	}
	
	public Turma(String name ) {
		this.name = name;
		p1 = p2 = p3 = 1;
	}
	
	public void addAluno( Aluno aluno ){
		alunos.add(aluno);
		resultado.add( aluno.nota.calculo(p1, p2, p3) );
	}
	
	public void setPeso( double p1, double p2, double p3 )
	{
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		for (int i = 0; i < alunos.size() ; i++) {
			this.resultado.set(i, alunos.get(i).nota.calculo(p1, p2, p3) );
		}
	}
	
	public void view()
	{
		for (int i = 0; i < alunos.size() ; i++) {
			System.out.println("Aluno: " + alunos.get(i).name + " Nota: " + resultado.get(i)  );
		}
		
	}
	
}
